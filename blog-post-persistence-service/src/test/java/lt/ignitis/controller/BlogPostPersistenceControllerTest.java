package lt.ignitis.controller;

import lt.ignitis.BlogPostPersistenceApplication;
import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.service.BlogPostPersistenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BlogPostPersistenceController.class)
@ContextConfiguration(classes = {BlogPostPersistenceApplication.class})
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
public class BlogPostPersistenceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BlogPostPersistenceController blogPostPersistenceController;

    @MockBean
    private BlogPostPersistenceService blogPostPersistenceService;

    @MockBean
    private BlogPostFormModel blogPostFormModel;

    @Test
    public void testBlogPostPersistenceControllerReturns400Error()
            throws Exception {
        when((blogPostPersistenceService).persistBlogPost(blogPostFormModel,
                1L)).thenReturn(1L);
        mockMvc.perform(get("/blogpost")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError());
    }

    @Test
    /*
     * JWT token in this test is of structure:
     *
     * {
     *   "alg": "HS256",
     * }.
     *  {
     *   "secret-user-id": 1234567,
     *   "iat": 1585772527764,
     *   "exp": 1585776127764"
     *  }.
     *   [signature] <-HS256((base64UrlEncode(header) "."
     *   base64UrlEncode(payload), secret-key)
     *
     * */
    public void testBlogPostPersistenceControllerReturnsOk() throws Exception {
        doReturn(1L).when(blogPostPersistenceService)
                .persistBlogPost(blogPostFormModel, 1L);
        mockMvc.perform(post("/blogpost")
                .header(HttpHeaders.AUTHORIZATION,
                        "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZX" +
                                "QtdXNlci1pZCI6MTIzNDU2NywiaWF0IjoxNTg1NzcyN" +
                                "TI3NzY0LCJleHAiOjE1ODU3NzYxMjc3NjR9.at2Apyw" +
                                "5E6FcKsm5wnPLUdbU_FF07pWHKykirvfo4bY")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "   \"title\":\"This is my title for this " +
                        "blogpost\",\n" +
                        "   \"content\":\"This is some random generated " +
                        "blog post content\"\n" +
                        "}")).andExpect(status().isOk());
    }

    @Test
    /*
     * JWT token in this test is of structure:
     *
     * {
     *   "alg": "HS256",
     * }.
     *  {
     *   "secret-user-id": 1234567,
     *   "iat": 1585772527764,
     *   "exp": 1585776127764"
     *  }.
     *   [signature] <-HS256((base64UrlEncode(header) "."
     *   base64UrlEncode(payload), secret-key)
     *
     * */
    public void testBlogPostPersistenceControllerInvokesBlogPostPersistenceService()
            throws Exception {
        doReturn(1L).when(blogPostPersistenceService)
                .persistBlogPost(blogPostFormModel, 1L);
        mockMvc.perform(post("/blogpost")
                .header(HttpHeaders.AUTHORIZATION,
                        "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZX" +
                                "QtdXNlci1pZCI6MTIzNDU2NywiaWF0IjoxNTg1NzcyN" +
                                "TI3NzY0LCJleHAiOjE1ODU3NzYxMjc3NjR9.at2Apyw" +
                                "5E6FcKsm5wnPLUdbU_FF07pWHKykirvfo4bY")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "   \"title\":\"This is my title for this" +
                        " blogpost\",\n" +
                        "   \"content\":\"This is some random generated " +
                        "blog post content\"\n" +
                        "}"));
        verify(blogPostPersistenceService, times(1))
                .persistBlogPost(any(BlogPostFormModel.class), any(Long.class));
    }
}