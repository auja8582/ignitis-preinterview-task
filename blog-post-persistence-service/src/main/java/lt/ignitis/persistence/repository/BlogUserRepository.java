package lt.ignitis.persistence.repository;

import lt.ignitis.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BlogUserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneById(Long userId);
}
