package lt.ignitis.persistence.service;

import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.model.BlogPost;
import lt.ignitis.persistence.repository.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BlogPostPersistenceService {

    private BlogPostRepository blogPostRepository;

    private UserService userService;

    @Autowired
    public BlogPostPersistenceService(BlogPostRepository blogPostRepository,
                                      UserService userService) {
        this.blogPostRepository = blogPostRepository;
        this.userService = userService;
    }

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public Long persistBlogPost(BlogPostFormModel blogPostFormModel,
                                Long userId) {
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(blogPostFormModel.getTitle());
        blogPost.setContent(blogPostFormModel.getContent());
        blogPost.setUser(userService.findUserById(userId));
        blogPostRepository.save(blogPost);
        return null;
    }

}
