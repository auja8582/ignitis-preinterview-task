package lt.ignitis.persistence.service;

import lt.ignitis.persistence.model.User;
import lt.ignitis.persistence.repository.BlogUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private BlogUserRepository blogUserRepository;

    @Autowired
    public UserService(BlogUserRepository blogUserRepository){
        this.blogUserRepository = blogUserRepository;
    }

    public User findUserById(Long userId){
       return blogUserRepository.findOneById(userId).orElseThrow();
    }
}
