package lt.ignitis.controller;

import io.jsonwebtoken.Jwts;
import io.swagger.annotations.ApiImplicitParam;
import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.service.BlogPostPersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BlogPostPersistenceController {

    private BlogPostPersistenceService blogPostPersistenceService;

    @Autowired
    public BlogPostPersistenceController(BlogPostPersistenceService
                                                 blogPostPersistenceService) {
        this.blogPostPersistenceService = blogPostPersistenceService;
    }

    @ApiImplicitParam(name="Authorization", value = "Bearer generated_token_from_login_service", required = true, dataType = "string", paramType = "header",
            example = "i.e Bearer eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZXQtdXNlci1pZCI6MSwiaWF0IjoxNTg1ODcyMTA1LCJleHAiOjE1ODU4NzU3MDV9.ozO7Zjf304fs3zFxYDr8Xys2ArhM92lgPERFJDNlJ4g")
    @CrossOrigin(origins = "*")
    @PostMapping(value = "/blogpost")
    public ResponseEntity<?> persistBlogPost(
            @RequestHeader(value = "Authorization") String token,
            @RequestBody BlogPostFormModel blogPostFormModel) {
        try {

            Long userId = Long.valueOf(Jwts.parser()
                    .setSigningKey("secret-key".getBytes())
                    .parseClaimsJws(token.split("\\s")[1])
                    .getBody()
                    .get("secret-user-id")
                    .toString());

            Long blogPostId = blogPostPersistenceService
                    .persistBlogPost(blogPostFormModel, userId);
            return ResponseEntity.ok(blogPostId);
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}