package lt.ignitis.persistence.model;

import javax.persistence.*;

@Entity
@Table(name="BLOG_POST")
public class BlogPost {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BLOG_USER_ID")
    private Long blogUserId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogUserId() {
        return blogUserId;
    }

    public void setBlogUserId(Long blogUserId) {
        this.blogUserId = blogUserId;
    }
}
