package lt.ignitis.persistence.service;

import lt.ignitis.persistence.exception.NoBlogPostFoundException;
import lt.ignitis.persistence.repository.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class BlogPostRemovalService {

    private BlogPostRepository blogPostRepository;

    @Autowired
    public BlogPostRemovalService(BlogPostRepository blogPostRepository) {
        this.blogPostRepository = blogPostRepository;
    }

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public void removeBlogPost(Long userId, Long blogPostId)
            throws NoBlogPostFoundException {
        blogPostRepository
                .findOneByBlogUserIdAndId(userId, blogPostId)
                .orElseThrow(NoBlogPostFoundException::new);

        blogPostRepository.deleteByBlogUserIdAndId(userId, blogPostId);
    }

}
