package lt.ignitis.controller;

import io.jsonwebtoken.Jwts;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lt.ignitis.persistence.exception.NoBlogPostFoundException;
import lt.ignitis.persistence.service.BlogPostRemovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BlogPostRemovalController {

    private BlogPostRemovalService blogPostRemovalService;

    @Autowired
    public BlogPostRemovalController(BlogPostRemovalService
                                                 blogPostRemovalService) {
        this.blogPostRemovalService = blogPostRemovalService;
    }

    @ApiImplicitParam(name="Authorization", value = "Bearer generated_token_from_login_service", required = true, dataType = "string", paramType = "header",
            example = "i.e Bearer eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZXQtdXNlci1pZCI6MSwiaWF0IjoxNTg1ODcyMTA1LCJleHAiOjE1ODU4NzU3MDV9.ozO7Zjf304fs3zFxYDr8Xys2ArhM92lgPERFJDNlJ4g")
    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/blogpost/{blogPostId}")
    public ResponseEntity<?> removeBlogPost(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable Long blogPostId) {
        try {

            Long userId = Long.valueOf(Jwts.parser()
                    .setSigningKey("secret-key".getBytes())
                    .parseClaimsJws(token.split("\\s")[1])
                    .getBody()
                    .get("secret-user-id")
                    .toString());

            blogPostRemovalService.removeBlogPost(userId, blogPostId);
            return ResponseEntity.ok().build();
        } catch (NoBlogPostFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.status(500).build();
        }
    }

}