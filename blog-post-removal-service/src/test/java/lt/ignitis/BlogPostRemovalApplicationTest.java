package lt.ignitis;

import lt.ignitis.controller.BlogPostRemovalController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
public class BlogPostRemovalApplicationTest {
    @Autowired
    private BlogPostRemovalController blogPostRemovalController;

    @Test
    public void testContextLoads() throws Exception {
        assertThat(blogPostRemovalController).isNotNull();
    }
}