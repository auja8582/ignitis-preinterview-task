package lt.ignitis.controller;

import lt.ignitis.BlogPostRemovalApplication;
import lt.ignitis.persistence.service.BlogPostRemovalService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BlogPostRemovalController.class)
@ContextConfiguration(classes = {BlogPostRemovalApplication.class})
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
class BlogPostRemovalControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BlogPostRemovalController blogPostRemovalController;

    @MockBean
    private BlogPostRemovalService blogPostRemovalService;

    @Test
    public void testBlogPostRetrievalControllerReturns400Error()
            throws Exception {
        doNothing().when(blogPostRemovalService)
                .removeBlogPost(1L, 1L);
        mockMvc.perform(delete("/blogpost")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError());
    }

    @Test
    /*
     * JWT token in this test is of structure:
     *
     * {
     *   "alg": "HS256",
     * }.
     *  {
     *   "secret-user-id": 1234567,
     *   "iat": 1585772527764,
     *   "exp": 1585776127764"
     *  }.
     *   [signature] <-HS256((base64UrlEncode(header) "."
     *   base64UrlEncode(payload), secret-key)
     *
     * */
    public void testBlogPostRemovalControllerReturnsOk() throws Exception {
        mockMvc.perform(delete("/blogpost/1")
                .contentType(MediaType.APPLICATION_JSON).header(
                        HttpHeaders.AUTHORIZATION,
                        "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZX" +
                                "QtdXNlci1pZCI6MTIzNDU2NywiaWF0IjoxNTg1NzcyN" +
                                "TI3NzY0LCJleHAiOjE1ODU3NzYxMjc3NjR9.at2Apyw" +
                                "5E6FcKsm5wnPLUdbU_FF07pWHKykirvfo4bY")
        ).andExpect(status().isOk());
    }

    @Test
    /*
     * JWT token in this test is of structure:
     *
     * {
     *   "alg": "HS256",
     * }.
     *  {
     *   "secret-user-id": 1234567,
     *   "iat": 1585772527764,
     *   "exp": 1585776127764"
     *  }.
     *   [signature] <-HS256((base64UrlEncode(header) "."
     *   base64UrlEncode(payload), secret-key)
     *
     * */
    public void testBlogPostRemovalControllerInvokesBlogPostRemovalService()
            throws Exception {
        doNothing().when(blogPostRemovalService)
                .removeBlogPost(1L, 1L);
        mockMvc.perform(delete("/blogpost/1").header(
                HttpHeaders.AUTHORIZATION,
                "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZX" +
                        "QtdXNlci1pZCI6MTIzNDU2NywiaWF0IjoxNTg1NzcyN" +
                        "TI3NzY0LCJleHAiOjE1ODU3NzYxMjc3NjR9.at2Apyw" +
                        "5E6FcKsm5wnPLUdbU_FF07pWHKykirvfo4bY"));
        verify(blogPostRemovalService, times(1))
                .removeBlogPost(any(Long.class), any(Long.class));
    }
}