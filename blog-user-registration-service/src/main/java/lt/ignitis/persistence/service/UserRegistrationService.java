package lt.ignitis.persistence.service;

import lt.ignitis.model.UserFormModel;
import lt.ignitis.persistence.exception.UserExistsException;
import lt.ignitis.persistence.model.User;
import lt.ignitis.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserRegistrationService {

    @Autowired
    private UserRepository userRepository;

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public UserFormModel registerUser(UserFormModel userFormModel)
            throws UserExistsException {

        if (userRepository.findOneByEmail(userFormModel.getEmail()).isPresent())
            throw new UserExistsException();

        User user = new User();
        user.setEmail(userFormModel.getEmail());
        user.setPassword(userFormModel.getPassword());
        user = userRepository.save(user);

        UserFormModel outputUserFormModel = new UserFormModel();
        outputUserFormModel.setEmail(user.getEmail());
        outputUserFormModel.setPassword(user.getPassword());
        outputUserFormModel.setId(user.getId());
        return outputUserFormModel;
    }

}
