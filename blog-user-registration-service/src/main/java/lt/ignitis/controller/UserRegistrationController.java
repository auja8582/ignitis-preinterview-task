package lt.ignitis.controller;

import lt.ignitis.model.UserFormModel;
import lt.ignitis.persistence.exception.UserExistsException;
import lt.ignitis.persistence.service.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserRegistrationController {

    @Autowired
    private UserRegistrationService userRegistrationService;

    @Autowired
    public UserRegistrationController(UserRegistrationService
                                                  userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/user")
    public ResponseEntity<?> registerUser(@RequestBody UserFormModel user) {
        try {
            UserFormModel userFormModel = userRegistrationService
                    .registerUser(user);
            return ResponseEntity.ok(userFormModel);
        } catch (UserExistsException e) {
            return ResponseEntity.status(409).build();
        } catch (Exception e) {
            return ResponseEntity.status(500).build();
        }
    }

}