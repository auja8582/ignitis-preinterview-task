package lt.ignitis.controller;

import lt.ignitis.UserRegistrationApplication;
import lt.ignitis.model.UserFormModel;
import lt.ignitis.persistence.exception.UserExistsException;
import lt.ignitis.persistence.service.UserRegistrationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserRegistrationController.class)
@ContextConfiguration(classes = {UserRegistrationApplication.class})
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
class UserRegistrationServiceTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRegistrationController userRegistrationController;

    @MockBean
    private UserFormModel userFormModel;

    @MockBean
    private UserRegistrationService userRegistrationService;

    @Test
    public void testUserRegistrationControllerReturns409Error()
            throws Exception {
        doThrow(UserExistsException.class).when(userRegistrationService)
                .registerUser(any(UserFormModel.class));
        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError());
    }

    @Test
    public void testUserRegistrationControllerReturnsOk() throws Exception {
        when((userRegistrationService).registerUser(new UserFormModel()))
                .thenReturn(new UserFormModel());
        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON).content("{\n" +
                        "   \"email\":\"test@test.com\",\n" +
                        "   \"password\":\"admin123\"\n" +
                        "}")
        ).andExpect(status().isOk());
    }

    @Test
    public void testUserRegistrationControllerInvokesUserRegistrationService()
            throws Exception {
        when((userRegistrationService).registerUser(new UserFormModel()))
                .thenReturn(new UserFormModel());
        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                "   \"email\":\"test@test.com\",\n" +
                "   \"password\":\"admin123\"\n" +
                "}"));
        verify(userRegistrationService, times(1))
                .registerUser(any(UserFormModel.class));
    }

}