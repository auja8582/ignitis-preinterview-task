package lt.ignitis;

import lt.ignitis.persistence.service.UserRegistrationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
public class UserRegistrationApplicationTest {

    @Autowired
    private UserRegistrationService userRegistrationService;

    @Test
    public void testContextLoads() throws Exception {
        assertThat(userRegistrationService).isNotNull();
    }
}