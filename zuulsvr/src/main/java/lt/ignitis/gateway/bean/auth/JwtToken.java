package lt.ignitis.gateway.bean.auth;

import javax.persistence.*;

@Entity
@Table(name = "JWT_TOKEN")
public class JwtToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BLOG_USER_ID")
    private Long blogUserId;

    private String token;

    public JwtToken(String token, Long blogUserId) {
        this.token = token;
        this.blogUserId = blogUserId;
    }

    public JwtToken(String token) {
        this.token = token;
    }

    public JwtToken() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBlogUserId() {
        return blogUserId;
    }

    public void setBlogUserId(Long blogUserId) {
        this.blogUserId = blogUserId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
