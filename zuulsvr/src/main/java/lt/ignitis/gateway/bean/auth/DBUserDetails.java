package lt.ignitis.gateway.bean.auth;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;

@JsonDeserialize(as = DBUserDetails.class)
public class DBUserDetails implements UserDetails
{

    private String email;
    private String password;
    private Integer active;
    private boolean isLocked;
    private boolean isExpired;
    private boolean isEnabled;

    public DBUserDetails(String email, String password, Integer active,
                         boolean isLocked, boolean isExpired,
                         boolean isEnabled) {
        this.email = email;
        this.password = password;
        this.active = active;
        this.isLocked = isLocked;
        this.isExpired = isExpired;
        this.isEnabled = isEnabled;
    }

    public DBUserDetails(String email, String password) {
        this.email = email;
        this.password = password;
        this.active = 1;
        this.isLocked = false;
        this.isExpired = false;
        this.isEnabled = true;
    }

    public DBUserDetails(String email) {
        this.email = email;
    }

    public DBUserDetails() {
        super();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new HashSet<>();
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return active==1;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !isLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !isExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
