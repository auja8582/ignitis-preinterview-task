package lt.ignitis.gateway.controller;

import io.swagger.annotations.ApiImplicitParam;
import lt.ignitis.gateway.bean.auth.AuthResponse;
import lt.ignitis.gateway.bean.auth.LoginRequest;
import lt.ignitis.gateway.exception.JwtTokenAlreadyExistsException;
import lt.ignitis.gateway.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private ILoginService iLoginService;

    @CrossOrigin("*")
    @PostMapping(value="/login")
    @ResponseBody
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest
                                                          loginRequest) {

        String token = iLoginService.login(loginRequest.getEmail(),
                loginRequest.getPassword());
        HttpHeaders headers = new HttpHeaders();
        List<String> headerlist = new ArrayList<>();
        List<String> exposeList = new ArrayList<>();
        headerlist.add("Content-Type");
        headerlist.add(" Accept");
        headerlist.add("X-Requested-With");
        headerlist.add("Authorization");
        headers.setAccessControlAllowHeaders(headerlist);
        exposeList.add("Authorization");
        headers.setAccessControlExposeHeaders(exposeList);
        headers.set("Authorization", token);
        return new ResponseEntity<AuthResponse>(new AuthResponse(token),
                headers, HttpStatus.CREATED);
    }

    @ApiImplicitParam(name="Authorization", value = "Bearer generated_token_from_login_service", required = true, dataType = "string", paramType = "header",
            example = "i.e Bearer eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZXQtdXNlci1pZCI6MSwiaWF0IjoxNTg1ODcyMTA1LCJleHAiOjE1ODU4NzU3MDV9.ozO7Zjf304fs3zFxYDr8Xys2ArhM92lgPERFJDNlJ4g")
    @CrossOrigin("*")
    @PostMapping(value="/logout")
    public ResponseEntity<AuthResponse> logout(
            @RequestHeader(value = "Authorization") String token) {
        HttpHeaders headers = new HttpHeaders();
        if (iLoginService.logout(token.split("\\s")[1])) {
            headers.remove("Authorization");
            return new ResponseEntity<AuthResponse>(
                    new AuthResponse("logged out"),
                    headers, HttpStatus.CREATED);
        }
        return new ResponseEntity<AuthResponse>(
                new AuthResponse("Logout Failed"), headers,
                HttpStatus.NOT_MODIFIED);
    }

}
