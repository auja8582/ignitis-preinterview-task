package lt.ignitis.gateway.repository;

import lt.ignitis.gateway.bean.auth.JwtToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JwtTokenRepository extends JpaRepository<JwtToken, Long> {
    Optional<JwtToken> findByToken(String Token);
    Optional<JwtToken> findOneByBlogUserIdAndToken(Long blogUserId, String token);
    Optional<JwtToken> findOneByBlogUserId(Long blogUserId);
    void deleteByBlogUserId(Long blogUserId);
}
