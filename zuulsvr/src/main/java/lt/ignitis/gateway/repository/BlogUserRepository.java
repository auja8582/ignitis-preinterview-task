package lt.ignitis.gateway.repository;

import lt.ignitis.gateway.bean.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BlogUserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByEmail(String email);

}
