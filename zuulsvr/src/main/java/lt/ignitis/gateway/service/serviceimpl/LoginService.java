package lt.ignitis.gateway.service.serviceimpl;

import lt.ignitis.gateway.bean.auth.JwtToken;
import lt.ignitis.gateway.bean.auth.User;
import lt.ignitis.gateway.exception.CustomException;
import lt.ignitis.gateway.repository.BlogUserRepository;
import lt.ignitis.gateway.repository.JwtTokenRepository;
import lt.ignitis.gateway.security.JwtTokenProvider;
import lt.ignitis.gateway.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class LoginService implements ILoginService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private BlogUserRepository userRepository;
    @Autowired
    private JwtTokenRepository jwtTokenRepository;

    @Override
    public String login(String email, String password){
        try {
            authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(email,
                    password));
            User user = userRepository.findOneByEmail(email).get();
            //NOTE: normally we dont need to add "ROLE_" prefix. Spring
            // does automatically for us.
            //Since we are using custom token using JWT we should add ROLE_
            // prefix
            String token = jwtTokenProvider.createToken(user.getId());
            return token;
        } catch (Exception e) {
            throw new CustomException("Invalid email or password.",
                    HttpStatus.UNAUTHORIZED);
        }
    }

    @Override
    @Transactional
    public boolean logout(String token) {
        jwtTokenRepository.deleteByBlogUserId(jwtTokenProvider.getSecretUserId(token));
        return true;
    }

    /*maybe someday use in controller*/
    @Override
    public Boolean isValidToken(String token) {
        return jwtTokenProvider.validateToken(token);
    }

    /*maybe someday use in controller*/
    @Override
    public String createNewToken(String token) {
        Long secretUserId = jwtTokenProvider.getSecretUserId(token);
        String newToken = jwtTokenProvider.createToken(secretUserId);
        return newToken;
    }
}
