package lt.ignitis.gateway.security;

import org.springframework.security.crypto.password.PasswordEncoder;

public class MyNoopPasswordEncoder implements PasswordEncoder {

    public MyNoopPasswordEncoder(Integer passwordStrength){

    }

    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return true;
    }

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return false;
    }
}
