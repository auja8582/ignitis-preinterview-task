package lt.ignitis.gateway.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lt.ignitis.gateway.bean.auth.JwtToken;
import lt.ignitis.gateway.repository.BlogUserRepository;
import lt.ignitis.gateway.repository.JwtTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;

@Component
public class JwtTokenProvider {
    private static final String AUTHORIZATION = "Authorization";
    private String secretKey = "secret-key";
    private long validityInMilliseconds = 3600000; // 1h

    @Autowired
    private JwtTokenRepository jwtTokenRepository;

    @Autowired
    private BlogUserRepository blogUserRepository;



    @Autowired
    private UserDetailsService userDetailsService;

    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    /*Let us make an assumption that user can have only one token*/
    public String createToken(Long userId) {

        jwtTokenRepository
                .findOneByBlogUserId(userId)
                .ifPresent((s)->jwtTokenRepository.delete(s));

            Claims claims = Jwts.claims();
            claims.put("secret-user-id", userId);
            Date now = new Date();
            Date validity = new Date(now.getTime() + validityInMilliseconds);

            String token = Jwts.builder()//
                    .setClaims(claims)//
                    .setIssuedAt(now)//
                    .setExpiration(validity)//
                    .signWith(SignatureAlgorithm.HS256, secretKey)//
                    .compact();

            jwtTokenRepository.save(new JwtToken(token, userId));

        return token;
    }

    public String resolveToken(HttpServletRequest req) {
        String bearerToken = req.getHeader(AUTHORIZATION);
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        if (bearerToken != null) {
            return bearerToken;
        }
        return null;
    }

    public boolean validateToken(String token) throws JwtException,
            IllegalArgumentException {
        Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        return true;
    }

    public boolean isTokenPresentInDB(String token) {
        return jwtTokenRepository
                .findOneByBlogUserIdAndToken(getSecretUserId(token), token)
                .isPresent();
    }

    public Long getSecretUserId(String token) {
        return Long.valueOf(Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .get("secret-user-id")
                .toString());
    }

    public String getEmail(String token) {
        return blogUserRepository
                .findById(getSecretUserId(token))
                .get()
                .getEmail();
    }

    public Authentication getAuthentication(String token) {
        //using data base: uncomment when you want to fetch data from data base
        UserDetails userDetails = userDetailsService
                .loadUserByUsername(getEmail(token));
        //from token take user value. comment below line for changing
        // it taking from data base
        return new UsernamePasswordAuthenticationToken(userDetails,
                "", userDetails.getAuthorities());
    }

}
