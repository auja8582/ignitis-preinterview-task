package lt.ignitis.gateway.security;

import lt.ignitis.gateway.bean.auth.DBUserDetails;
import lt.ignitis.gateway.bean.auth.User;
import lt.ignitis.gateway.exception.CustomException;
import lt.ignitis.gateway.repository.BlogUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService{

    @Autowired
    private BlogUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        User user = userRepository.findOneByEmail(email)
                .orElseThrow(()-> new CustomException(
                        "Invalid username or password.",
                        HttpStatus.UNAUTHORIZED));

        DBUserDetails userDetails = new DBUserDetails(
                user.getEmail(),user.getPassword());
        return userDetails;
    }



}
