package lt.ignitis.controller;

import io.jsonwebtoken.Jwts;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.exception.NoBlogPostFoundException;
import lt.ignitis.persistence.service.BlogPostUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class BlogPostUpdateController {

    private BlogPostUpdateService blogPostUpdateService;

    @Autowired
    public BlogPostUpdateController(BlogPostUpdateService
                                                blogPostUpdateService) {
        this.blogPostUpdateService = blogPostUpdateService;
    }
    @ApiImplicitParam(name="Authorization", value = "Bearer generated_token_from_login_service", required = true, dataType = "string", paramType = "header",
    example = "i.e Bearer eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZXQtdXNlci1pZCI6MSwiaWF0IjoxNTg1ODcyMTA1LCJleHAiOjE1ODU4NzU3MDV9.ozO7Zjf304fs3zFxYDr8Xys2ArhM92lgPERFJDNlJ4g")
    @CrossOrigin(origins = "*")
    @PutMapping(value = "/blogpost/{blogPostId}")
    public ResponseEntity<?> updateBlogPost(
            @RequestBody BlogPostFormModel blogPostFormModel,
            @RequestHeader(value = "Authorization") String token,
            @PathVariable Long blogPostId) {
        try {

            Long userId = Long.valueOf(Jwts.parser()
                    .setSigningKey("secret-key".getBytes())
                    .parseClaimsJws(token.split("\\s")[1])
                    .getBody()
                    .get("secret-user-id")
                    .toString());

            blogPostUpdateService.updateBlogPost(userId, blogPostId,
                    blogPostFormModel);
            return ResponseEntity.ok().build();
        }catch (NoBlogPostFoundException e){
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}