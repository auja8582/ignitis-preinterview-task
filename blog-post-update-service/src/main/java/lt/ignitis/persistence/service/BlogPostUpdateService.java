package lt.ignitis.persistence.service;

import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.exception.NoBlogPostFoundException;
import lt.ignitis.persistence.model.BlogPost;
import lt.ignitis.persistence.repository.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BlogPostUpdateService {

    private BlogPostRepository blogPostRepository;

    @Autowired
    public BlogPostUpdateService(BlogPostRepository blogPostRepository) {
        this.blogPostRepository = blogPostRepository;
    }

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public void updateBlogPost(Long blogUserId, Long blogPostId,
                               BlogPostFormModel blogPostFormModel)
            throws NoBlogPostFoundException {
        BlogPost blogPost = blogPostRepository
                .findOneByIdAndBlogUserId(blogPostId, blogUserId)
                .orElseThrow(NoBlogPostFoundException::new);
        blogPost.setTitle(blogPostFormModel.getTitle());
        blogPost.setContent(blogPostFormModel.getContent());
        blogPostRepository.save(blogPost);
    }
}
