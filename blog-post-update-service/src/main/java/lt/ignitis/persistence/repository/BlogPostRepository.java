package lt.ignitis.persistence.repository;

import lt.ignitis.persistence.model.BlogPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BlogPostRepository extends JpaRepository<BlogPost, Long> {
    Optional<BlogPost> findOneById(Long id);
    Optional<BlogPost> findOneByIdAndBlogUserId(Long blogPostId, Long blogUserId);

}
