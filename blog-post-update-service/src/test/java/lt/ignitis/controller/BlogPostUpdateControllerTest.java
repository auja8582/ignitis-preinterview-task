package lt.ignitis.controller;

import lt.ignitis.BlogPostUpdateApplication;
import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.service.BlogPostUpdateService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(BlogPostUpdateController.class)
@ContextConfiguration(classes = {BlogPostUpdateApplication.class})
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
class BlogPostUpdateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private BlogPostUpdateController blogPostUpdateController;

    @MockBean
    private BlogPostFormModel blogPostFormModel;

    @MockBean
    private BlogPostUpdateService blogPostUpdateService;

    @Test
    public void testBlogPostUpdateControllerReturns400Error()
            throws Exception {
        doNothing().when(blogPostUpdateService)
                .updateBlogPost(1L, 1L, blogPostFormModel);
        mockMvc.perform(put("/blogpost/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is4xxClientError());
    }

    @Test
    /*
     * JWT token in this test is of structure:
     *
     * {
     *   "alg": "HS256",
     * }.
     *  {
     *   "secret-user-id": 1234567,
     *   "iat": 1585772527764,
     *   "exp": 1585776127764"
     *  }.
     *   [signature] <-HS256((base64UrlEncode(header) "."
     *   base64UrlEncode(payload), secret-key)
     *
     * */
    public void testBlogPostUpdateControllerReturnsOk() throws Exception {
        doNothing().when(blogPostUpdateService)
                .updateBlogPost(1L, 1L, blogPostFormModel);
        mockMvc.perform(put("/blogpost/1")
                .contentType(MediaType.APPLICATION_JSON).header(
                        HttpHeaders.AUTHORIZATION,
                        "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZX" +
                                "QtdXNlci1pZCI6MTIzNDU2NywiaWF0IjoxNTg1NzcyN" +
                                "TI3NzY0LCJleHAiOjE1ODU3NzYxMjc3NjR9.at2Apyw" +
                                "5E6FcKsm5wnPLUdbU_FF07pWHKykirvfo4bY")
                .content("{\n" +
                        "   \"title\":\"This is my title for this " +
                        "blogpost\",\n" +
                        "   \"content\":\"This is some random generated blog " +
                        "post content\"\n" +
                        "}")
        ).andExpect(status().isOk());
    }

    @Test
    /*
     * JWT token in this test is of structure:
     *
     * {
     *   "alg": "HS256",
     * }.
     *  {
     *   "secret-user-id": 1234567,
     *   "iat": 1585772527764,
     *   "exp": 1585776127764"
     *  }.
     *   [signature] <-HS256((base64UrlEncode(header) "."
     *   base64UrlEncode(payload), secret-key)
     *
     * */
    public void testTaskUpdateControllerInvokesTaskUpdateService()
            throws Exception {
        doNothing().when(blogPostUpdateService)
                .updateBlogPost(1L, 1L, blogPostFormModel);
        mockMvc.perform(put("/blogpost/1")
                .contentType(MediaType.APPLICATION_JSON).header(
                        HttpHeaders.AUTHORIZATION,
                        "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZX" +
                                "QtdXNlci1pZCI6MTIzNDU2NywiaWF0IjoxNTg1NzcyN" +
                                "TI3NzY0LCJleHAiOjE1ODU3NzYxMjc3NjR9.at2Apyw" +
                                "5E6FcKsm5wnPLUdbU_FF07pWHKykirvfo4bY")

                        .content("{\n" +
                                "   \"title\":\"This is my title for this " +
                                "blogpost\",\n" +
                                "   \"content\":\"This is some random " +
                                "generated blog post content\"\n" +
                                "}"));
        verify(blogPostUpdateService, times(1))
                .updateBlogPost(any(Long.class), any(Long.class),
                        any(BlogPostFormModel.class));
    }

}