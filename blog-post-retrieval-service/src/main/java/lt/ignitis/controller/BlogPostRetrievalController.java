package lt.ignitis.controller;

import io.jsonwebtoken.Jwts;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.service.BlogPostRetrievalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlogPostRetrievalController {

    private BlogPostRetrievalService blogPostRetrievalService;

    @Autowired
    public BlogPostRetrievalController(BlogPostRetrievalService
                                                   blogPostRetrievalService) {
        this.blogPostRetrievalService = blogPostRetrievalService;
    }

    @ApiImplicitParam(name="Authorization", value = "Bearer generated_token_from_login_service", required = true, dataType = "string", paramType = "header",
            example = "i.e Bearer eyJhbGciOiJIUzI1NiJ9.eyJzZWNyZXQtdXNlci1pZCI6MSwiaWF0IjoxNTg1ODcyMTA1LCJleHAiOjE1ODU4NzU3MDV9.ozO7Zjf304fs3zFxYDr8Xys2ArhM92lgPERFJDNlJ4g")
    @CrossOrigin(origins = "*")
    @GetMapping(value = "/blogpost")
    public ResponseEntity<?> getBlogPosts(
                      @RequestHeader(value = "Authorization") String token) {
        try {
            Long userId = Long.valueOf(Jwts.parser()
                    .setSigningKey("secret-key".getBytes())
                    .parseClaimsJws(token.split("\\s")[1])
                    .getBody()
                    .get("secret-user-id")
                    .toString());
            List<BlogPostFormModel> blogPostFormModel = blogPostRetrievalService
                    .getBlogPosts(userId);
                return ResponseEntity.ok(blogPostFormModel);
        } catch (Exception e){
            return ResponseEntity.status(500).build();
        }
    }

}