package lt.ignitis.persistence.service;

import lt.ignitis.model.BlogPostFormModel;
import lt.ignitis.persistence.model.BlogPost;
import lt.ignitis.persistence.repository.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BlogPostRetrievalService {

    @Autowired
    private BlogPostRepository blogPostRepository;

    /**
     * TODO: add aspectj weaving and refactor to private methods
     */
    @Transactional
    public List<BlogPostFormModel> getBlogPosts(Long blogUserId) {
        return blogPostRepository.findAllByBlogUserId(blogUserId)
                .stream()
                .map(this::convertBlogPostToBlogPostFormModel)
                .collect(Collectors.toList());
    }

    private BlogPostFormModel convertBlogPostToBlogPostFormModel(BlogPost blogPost) {
        BlogPostFormModel blogPostFormModel = new BlogPostFormModel();
        blogPostFormModel.setTitle(blogPost.getTitle());
        blogPostFormModel.setContent(blogPost.getContent());
        blogPostFormModel.setId(blogPost.getId());
        return blogPostFormModel;
    }

}
