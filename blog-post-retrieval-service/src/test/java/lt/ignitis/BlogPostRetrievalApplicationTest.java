package lt.ignitis;

import lt.ignitis.persistence.service.BlogPostRetrievalService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@EnableAutoConfiguration(exclude={LiquibaseAutoConfiguration.class})
public class BlogPostRetrievalApplicationTest {

    @Autowired
    private BlogPostRetrievalService blogPostRetrievalService;

    @Test
    public void testContextLoads() throws Exception {
        assertThat(blogPostRetrievalService).isNotNull();
    }
}